import React, { Component } from "react";
import { connect } from "react-redux";
import baiTapOanTuXi from "./redux/reducer/BaiTapOanTuXiReducer";
import { OAN_TU_XI } from "./redux/constants/OanTuXiConstants";

class Player extends Component {
  render() {
    return (
      <div>
        <div className="theThink">
          <img
            style={{ transform: `rotate(180deg)` }}
            className="mt-4"
            width={50}
            src={this.props.mangDatCuoc.find((item) => item.datCuoc).hinhAnh}
            alt=""
          />
        </div>
        <div className="speech-bubble"></div>
        <img
          style={{ width: 150, height: 150 }}
          src="./img/gameOanTuXi/player.png"
          alt=""
        />
        <div className="d-flex ml-5">
          {this.props.mangDatCuoc.map((item, index) => {
            let border = {};
            if (item.datCuoc) {
              border = { border: "3px solid orange" };
            }
            return (
              <div key={index} className="p-0">
                <button
                  onClick={() => {
                    this.props.datCuoc(item.ma);
                  }}
                  style={border}
                  className="ml-5 btnItem"
                >
                  <img width={35} height={35} src={item.hinhAnh} alt="" />
                </button>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    mangDatCuoc: state.baiTapOanTuXi.mangDatCuoc,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (maCuoc) => {
      dispatch({
        type: OAN_TU_XI,
        maCuoc,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Player);
