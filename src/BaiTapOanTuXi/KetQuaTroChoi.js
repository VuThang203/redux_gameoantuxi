import React, { Component } from "react";
import { connect } from "react-redux";
import baiTapOanTuXi from "./redux/reducer/BaiTapOanTuXiReducer";

class KetQuaTroChoi extends Component {
  render() {
    return (
      <div>
        <div>
          <p className="display-4 text-warning">
            I'm iron man, i love you 3000
          </p>
          <p className="text-success text__result">{this.props.ketQua}</p>
          <p className="text-primary">Số bàn thắng: {this.props.soBanThang}</p>
          <p className="text-danger">Số lượt chơi: {this.props.soLuotChoi}</p>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    ketQua: state.baiTapOanTuXi.ketQua,
    soBanThang: state.baiTapOanTuXi.soBanThang,
    soLuotChoi: state.baiTapOanTuXi.soLuotChoi,
  };
};

export default connect(mapStateToProps)(KetQuaTroChoi);
