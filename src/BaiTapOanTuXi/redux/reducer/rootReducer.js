import { combineReducers } from "redux";
import baiTapOanTuXi from "./BaiTapOanTuXiReducer";

const rootReducer = combineReducers({
  baiTapOanTuXi,
});

export default rootReducer;
