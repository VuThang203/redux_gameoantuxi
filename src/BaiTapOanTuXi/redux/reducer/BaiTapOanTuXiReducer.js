import { OAN_TU_XI, RAN_DOM, END_GAME } from "../constants/OanTuXiConstants";
const initialState = {
  mangDatCuoc: [
    {
      ma: "keo",
      hinhAnh: "./img/gameOanTuXi/keo.png",
      datCuoc: true,
    },
    {
      ma: "bua",
      hinhAnh: "./img/gameOanTuXi/bua.png",
      datCuoc: false,
    },
    {
      ma: "bao",
      hinhAnh: "./img/gameOanTuXi/bao.png",
      datCuoc: false,
    },
  ],
  ketQua: " Bạn Thắng!!!",
  soBanThang: 0,
  soLuotChoi: 0,
  computer: {
    ma: "keo",
    hinhAnh: "./img/gameOanTuXi/keo.png",
  },
};

const baiTapOanTuXi = (state = initialState, action) => {
  switch (action.type) {
    case OAN_TU_XI: {
      let newMangDatCuoc = [...state.mangDatCuoc];
      newMangDatCuoc = newMangDatCuoc.map((item, index) => {
        return { ...item, datCuoc: false };
      });
      let index = newMangDatCuoc.findIndex((item) => item.ma == action.maCuoc);
      if (index !== -1) {
        newMangDatCuoc[index].datCuoc = true;
      }
      state.mangDatCuoc = newMangDatCuoc;
      return { ...state };
    }
    // So sánh mã cược để thay đổi hình ảnh
    case RAN_DOM: {
      let soNgauNhien = Math.floor(Math.random() * 3);
      let tuXiNgauNhien = state.mangDatCuoc[soNgauNhien];
      state.computer = tuXiNgauNhien;
      return { ...state };
    }
    case END_GAME:
      {
        let player = state.mangDatCuoc.find((item) => item.datCuoc === true);
        let computer = state.computer;
        switch (player.ma) {
          case "keo":
            if (computer.ma === "keo") {
              state.ketQua = "hoà rồi !!!";
            } else if (computer.ma === "bua") {
              state.ketQua = "bạn thua !!!";
            } else {
              state.soBanThang += 1;
              state.ketQua = `I'm iron man, i love you 3000`;
            }
            break;
          case "bua":
            if (computer.ma === "keo") {
              state.ketQua = "thua rồi !!!";
            } else if (computer.ma === "bua") {
              state.ketQua = "hoà rồi !!!";
            } else {
              state.soBanThang += 1;
              state.ketQua = `I'm iron man, i love you 3000`;
            }
            break;
          case "bao":
            if (computer.ma === "keo") {
              state.ketQua = "thua rồi !!!";
            } else if (computer.ma === "bua") {
              state.soBanThang += 1;
              state.ketQua = `I'm iron man, i love you 3000`;
            } else {
              state.ketQua = `hoà rồi !!!`;
            }
            break;
            return { ...state };
          default:
            break;
        }
      }
      state.soLuotChoi += 1;
    default:
      return { ...state };
  }
};
export default baiTapOanTuXi;
