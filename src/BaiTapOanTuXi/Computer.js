import React, { Component } from "react";
import { connect } from "react-redux";
import baiTapOanTuXi from "./redux/reducer/BaiTapOanTuXiReducer";

class Computer extends Component {
  render() {
    let keyframe = ` @keyframes randomItem${Date.now()} {
      0% {top: -50px;}
      25% {top: 100px;}
      50% {top: -50px;}
      75% {top: 100px;}
      100% {top: 0;}
    }`;
    return (
      <div>
        <style>{keyframe}</style>
        <div
          className="theThink"
          style={{ position: `relative`, overflow: `hidden` }}
        >
          <img
            style={{
              position: `absolute`,
              left: "35%",
              animation: `randomItem${Date.now()} 1s`,
            }}
            className="mt-4"
            width={50}
            src={this.props.computer.hinhAnh}
            alt=""
          />
        </div>
        <div className="speech-bubble"></div>
        <img
          style={{ width: 150, height: 150 }}
          src="./img/gameOanTuXi/playerComputer.png"
          alt=""
        />
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    computer: state.baiTapOanTuXi.computer,
  };
};

export default connect(mapStateToProps)(Computer);
