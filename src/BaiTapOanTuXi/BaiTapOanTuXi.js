import React, { Component } from "react";
import "./BaiTapOanTuXi.css";
import Player from "./Player";
import Computer from "./Computer";
import KetQuaTroChoi from "./KetQuaTroChoi";
import { connect } from "react-redux";
import { RAN_DOM, END_GAME } from "./redux/constants/OanTuXiConstants";
class BaiTapOanTuXi extends Component {
  render() {
    return (
      <div className="gameOanTuXi">
        <div className="row mt-5">
          <div className="col-4">
            <Player />
          </div>
          <div className="col-4">
            <KetQuaTroChoi />
            <button
              onClick={() => {
                this.props.playGame();
              }}
              className="btn btn-success"
            >
              Play game
            </button>
          </div>
          <div className="col-4">
            <Computer />
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProp = (dispatch) => {
  return {
    playGame: () => {
      let count = 0;
      let randomInterval = setInterval(() => {
        dispatch({
          type: RAN_DOM,
        });
        count++;
        if (count > 5) {
          clearInterval(randomInterval);
          dispatch({
            type: END_GAME,
          });
        }
      }, 100);
    },
  };
};

export default connect(null, mapDispatchToProp)(BaiTapOanTuXi);
