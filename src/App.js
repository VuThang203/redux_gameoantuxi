import logo from "./logo.svg";
import "./App.css";
import GioHangRedux from "./GioHangRedux/GioHangRedux.jsx";
import BaiTapOanTuXi from "./BaiTapOanTuXi/BaiTapOanTuXi";
function App() {
  return (
    <div className="App">
      {/* <GioHangRedux /> */}
      <BaiTapOanTuXi />
    </div>
  );
}

export default App;
