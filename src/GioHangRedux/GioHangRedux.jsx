import React, { Component } from "react";
import ProductList from "./ProductList.jsx";
import Cart from "./Cart";
export default class GioHangRedux extends Component {
  render() {
    return (
      <div>
        <h3>Danh sách sản phẩm</h3>
        <div className="container">
          <div className="text-right">
            <span
              style={{ width: 17, cursor: "pointer" }}
              data-toggle="modal"
              data-target="#modelId"
            >
              <i className="fa fa-cart mr-5">
                <i className="fa fa-cart-arrow-down">(0) Giỏ hàng</i>
              </i>
            </span>
          </div>
          <ProductList />
          <Cart />
        </div>
      </div>
    );
  }
}
