import React, { Component } from "react";
import { connect } from "react-redux";

 class ProductItem extends Component {
  render() {
    let { product } = this.props;
    return (
      <div className="card h-100 text-left">
        <img className="card-img-top" src={product.hinhAnh} alt="" />
        <div className="card-body">
          <h4 className="card-title">{product.tenSP}</h4>
          <p className="card-text">{product.giaBan.toLocaleString()} VND</p>
          <button className="btn btn-success">Thêm sản phẩm </button>
        </div>
      </div>
    );
  }
}

export default connect ()(ProductItem)